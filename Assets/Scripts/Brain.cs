﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Brain : MonoBehaviour
{
    [SerializeField] private GameObject eyes;

    private int dnaLength;
    private int geneValues;

    private DNA dna;
    private DNA speedDna;
    private DNA rotationDna;

    private bool alive;

    private Vector3 startingPosition;
    private bool touchingWall;
    private int touchingWallRight;
    private int touchingWallLeft;

    private float reward;

    public DNA Dna { get => dna; }
    public float Reward { get => reward; }
    public Vector3 StartingPosition { get => startingPosition; }
    public DNA SpeedDna { get => speedDna; set => speedDna = value; }
    public DNA RotationDna { get => rotationDna; set => rotationDna = value; }

    private void Awake()
    {
        alive = true;
        touchingWall = false;
        touchingWallLeft = 0;
        touchingWallRight = 0;
        startingPosition = this.transform.position;
        dnaLength = 8;
        geneValues = 3;
        reward = 100;
        dna = new DNA(dnaLength, geneValues);
        speedDna = new DNA(1, 7);
        rotationDna = new DNA(1, 360);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "dead")
        {
            alive = false;
            reward = -1000000000;
        }
    }

    private void Update()
    {
        if (!alive)
            return;

        touchingWall = false;
        touchingWallLeft = 0;
        touchingWallRight = 0;
        RaycastHit hit;
        RaycastHit hitRight;
        RaycastHit hitLeft;
        RaycastHit hitBack;
        bool ray = Physics.Raycast(eyes.transform.position, eyes.transform.forward, out hit, .5f);
        bool rayBack = Physics.Raycast(eyes.transform.position, -eyes.transform.forward, out hitBack, .285f);
        bool rayLeft = Physics.Raycast(eyes.transform.position, -eyes.transform.right, out hitLeft, .285f);
        bool rayRight = Physics.Raycast(eyes.transform.position, eyes.transform.right, out hitRight, .285f);

        if (ray && hit.collider.tag.Equals("wall"))
        {
            touchingWall = true;
            reward -= 0.01f;
        }
        else
            ++reward;

        if (rayLeft && hitLeft.collider.tag.Equals("wall"))
        {
            touchingWallLeft = 1;
            reward -= 0.01f;
        }
        else
            ++reward;

        if (rayRight && hitRight.collider.tag.Equals("wall"))
        {
            touchingWallRight = 1;
            reward -= 0.01f;
        }
        else
            ++reward;

        if (rayBack && hitBack.collider.tag.Equals("wall"))
        {
            reward -= 1.0f;
        }
        else
            ++reward;
    }

    private void FixedUpdate()
    {
        Vector3 forewardSpeed = Vector3.zero;
        float rotation = 0.0f;
        if (!touchingWall)
        {
            if (touchingWallLeft + touchingWallRight == 2)
            {
                DoAction(ref forewardSpeed, ref rotation, 5);
            }
            else if (touchingWallLeft == 1)
            {
                DoAction(ref forewardSpeed, ref rotation, 6);
            }
            else if (touchingWallRight == 1)
            {
                DoAction(ref forewardSpeed, ref rotation, 7);
            }
            else
            {
                DoAction(ref forewardSpeed, ref rotation, 0);
            }
        }
        else
        {
            if (touchingWallLeft + touchingWallRight == 2)
            {
                DoAction(ref forewardSpeed, ref rotation, 4);
            }
            else if (touchingWallLeft == 1)
            {
                DoAction(ref forewardSpeed, ref rotation, 2);
            }
            else if (touchingWallRight == 1)
            {
                DoAction(ref forewardSpeed, ref rotation, 3);
            }
            else
            {
                DoAction(ref forewardSpeed, ref rotation, 1);
            }
        }

        reward += forewardSpeed.magnitude;
        this.transform.Translate(forewardSpeed * Time.deltaTime * 7);
        this.transform.Rotate(0, rotation, 0);
    }

    private void DoAction(ref Vector3 forewardSpeed, ref float rotation, int genePosition)
    {
        switch (dna.GetGene(genePosition))
        {
            case 0:
                switch (speedDna.GetGene(0))
                {
                    case 0:
                        forewardSpeed = Vector3.forward;
                        break;
                    case 1:
                        forewardSpeed = Vector3.right;
                        break;
                    case 2:
                        forewardSpeed = -Vector3.right;
                        break;
                    case 3:
                        forewardSpeed = Vector3.right + Vector3.forward;
                        break;
                    case 4:
                        forewardSpeed = Vector3.right - Vector3.forward;
                        break;
                    case 5:
                        forewardSpeed = Vector3.left + Vector3.forward;
                        break;
                    case 6:
                        forewardSpeed = Vector3.left - Vector3.forward;
                        break;
                }
                break;
            case 1:
                rotation = rotationDna.GetGene(0);
                break;
            case 2:
                rotation = -rotationDna.GetGene(0);
                break;
        }
    }
}
