﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DNA
{
    private List<int> genes;

    private int dnaLength;
    private int genesPossibleValues;

    public List<int> Genes { get => genes; }

    public DNA (int dnaLength, int genePossibleValues)
    {
        genes = new List<int>();
        this.dnaLength = dnaLength;
        this.genesPossibleValues = genePossibleValues;
        GenerateRandomGenes();
    }

    private void GenerateRandomGenes()
    {
        for (int i = 0; i < dnaLength; i++)
        {
            genes.Add(UnityEngine.Random.Range(0, genesPossibleValues));
        }
    }

    public void CombineParentsGene(DNA father, DNA mother)
    {
        for (int i = 0; i < dnaLength; i++)
        {
            if (i < dnaLength / 2)
                genes[i] = father.GetGene(i);
            else
                genes[i] = mother.GetGene(i);
        }
    }

    public int GetGene(int position)
    {
        return genes[position];
    }

    public void Mutate()
    {
        genes[UnityEngine.Random.Range(0, genes.Count)] = UnityEngine.Random.Range(0, genesPossibleValues);
    }
    
    public void SetGene(int position, int value)
    {
        genes[position] = value;
    }
}
