﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class PopulationManager : MonoBehaviour
{
    [SerializeField] private GameObject person;
    [SerializeField] private int populationSize = 50;

    [SerializeField] private float sessionTimer = 10.0f;

    private List<GameObject> population = new List<GameObject>();

    private int generation = 0;
    private float elapsed = 0.0f;

    GUIStyle guiStyle = new GUIStyle();
    private void OnGUI()
    {
        guiStyle.fontSize = 25;
        guiStyle.normal.textColor = Color.white;
        GUI.BeginGroup(new Rect(10, 10, 250, 150));
        GUI.Box(new Rect(0, 0, 140, 140), "Stats ", guiStyle);
        GUI.Label(new Rect(10, 25, 200, 30), "Generation: " + generation, guiStyle);
        GUI.Label(new Rect(10, 50, 200, 30), string.Format("Time {0:0.00}", elapsed), guiStyle);
        GUI.Label(new Rect(10, 75, 200, 30), "Population: " + population.Count, guiStyle);
        GUI.EndGroup();
    }

    private void Start()
    {
        GeneratePopulation();
    }

    private void GeneratePopulation()
    {
        ++generation;

        population.Clear();

        for (int i = 0; i < populationSize; i++)
        {
            GameObject child = Instantiate(person, this.transform.position, Quaternion.identity);
            population.Add(child);
        }

        elapsed = 0.0f;
    }

    private void Update()
    {
        elapsed += Time.deltaTime;
        if (elapsed >= sessionTimer)
            SpawnNewPopulation();
    }

    private GameObject Breed(GameObject father, GameObject mother)
    {
        Vector3 position = new Vector3(this.transform.position.x + UnityEngine.Random.Range(-1.0f,1.0f), this.transform.position.y, this.transform.position.z + UnityEngine.Random.Range(-1.0f, 1.0f));

        GameObject child = Instantiate(person, position, Quaternion.identity);
        var childBrain = child.GetComponent<Brain>();
        if (UnityEngine.Random.Range(0, 100) == 0)
            childBrain.Dna.Mutate();
        else
            childBrain.Dna.CombineParentsGene(father.GetComponent<Brain>().Dna, mother.GetComponent<Brain>().Dna);
        childBrain.SpeedDna = father.GetComponent<Brain>().Reward > mother.GetComponent<Brain>().Reward ? father.GetComponent<Brain>().SpeedDna : mother.GetComponent<Brain>().SpeedDna;
        childBrain.RotationDna = father.GetComponent<Brain>().Reward > mother.GetComponent<Brain>().Reward ? father.GetComponent<Brain>().RotationDna : mother.GetComponent<Brain>().RotationDna;
        return child;
    }

    private void SpawnNewPopulation()
    {
        elapsed = 0.0f;
        ++generation;

        List<GameObject> anthems = new List<GameObject>();
        anthems = population.OrderBy(e => (e.transform.position.magnitude - e.GetComponent<Brain>().StartingPosition.magnitude) * e.GetComponent<Brain>().Reward).ToList();


        population.Clear();
        population.ForEach(e => Destroy(e));

        for (int i = (anthems.Count / 2) - 1; i < anthems.Count - 1; i++)
        {
            population.Add(Breed(anthems[i], anthems[i+1]));
            population.Add(Breed(anthems[i+1], anthems[i]));
        }

        anthems.ForEach(e => Destroy(e));
    }
}
